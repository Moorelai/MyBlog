/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.dao;

import com.moore.blog.po.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
  * MOORE
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月07日 2:03 下午
  * @version V1.0.0
  */

public interface UserRepository extends JpaRepository<User,Long> {

    /**
     *  用户名称和密码
     * @param user_name
     * @param password
     * @return
     */


     User findByUserNameAndPassword(String user_name, String password);
}

