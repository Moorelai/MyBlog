/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.dao;
import com.moore.blog.po.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

/**
  * MOORE
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月16日 11:18 下午
  * @version V1.0.0
  */
public interface TagRepository extends JpaRepository<Tag,Long> {
    /**
     * 标签名称
     * @param name
     * @return
     */
    Tag findByName(String name);

}

   