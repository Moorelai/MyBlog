/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.dao;
import com.moore.blog.po.Type;
import org.springframework.data.jpa.repository.JpaRepository;

/**
  * MOORE
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月16日 11:18 下午
  * @version V1.0.0
  */
public interface TypeRepository extends JpaRepository<Type,Long> {
    /**
     * 名称
     * @param name
     * @return
     */
    Type findByName(String name);
}

   