/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.lang.reflect.Constructor;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年02月28日 10:24 下午
  * @version V1.0.0
  */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
