/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.aspect;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.weaver.JoinPointSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月01日 1:00 下午
  * @version V1.0.0
  */
@Aspect
@Component
public class  LogAspect {
//   日志记录器
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 定义切面 *》所有，web底下的类定义一个切面
     */
    @Pointcut("execution(* com.moore.blog.web.*.*(..))")
    public void log(){
    }
   @Before("log()")
    public void  doBefore(JoinPoint joinpoint){
       ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
       HttpServletRequest request = attributes.getRequest();
       String url = request.getRequestURL().toString();
       String ip = request.getRemoteAddr();
       String classMethod = joinpoint.getSignature().getDeclaringTypeName()+"."+joinpoint.getSignature().getName();
       Object[] args = joinpoint.getArgs();
       RequestLog requestLog = new RequestLog(url,ip,classMethod,args);


       logger.info("Request:{}",requestLog);
    }

    @After("log()")
    public void  doAfter(){

//    logger.info("----doAfter-");

    }

    @AfterReturning(returning =  "result",pointcut = "log()")
     public  void doAfterReturn(Object result){
        logger.info("Result:{}",result);
     }

    private class RequestLog{
        private String url;
        private String ip;
        private String classMethod;
        private Object[] args;

        public RequestLog(String url, String ip, String classMethod, Object[] args) {
            this.url = url;
            this.ip = ip;
            this.classMethod = classMethod;
            this.args = args;
        }

        @Override
        public String toString() {
            return "RequestLog{" +
                    "url='" + url + '\'' +
                    ", ip='" + ip + '\'' +
                    ", classMethod='" + classMethod + '\'' +
                    ", args=" + Arrays.toString(args) +
                    '}';
        }
    }
}
