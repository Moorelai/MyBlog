/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.po;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月04日 10:58 下午
  * @version V1.0.0
  */
@Entity
@Table(name="t_Tag")
public class Tag {

    @Id
    @GeneratedValue
    private  Long id;

    private  String name;

    @ManyToMany(mappedBy = "tags")
    private List<Blog> blogs = new ArrayList<>();

    public Tag() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }



    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
