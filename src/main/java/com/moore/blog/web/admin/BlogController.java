/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.web.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月11日 9:51 下午
  * @version V1.0.0
  */
@Controller
@RequestMapping("/admin")
public class BlogController   {
     @GetMapping("/bolgs")
    public String bolgs (){
         return "admin/bolgs";
    }
}
