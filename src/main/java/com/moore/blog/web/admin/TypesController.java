/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.web.admin;

import com.moore.blog.po.Type;
import com.moore.blog.service.TypeService;
import com.sun.webkit.graphics.WCPageBackBuffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jws.Oneway;
import javax.jws.WebParam;
import javax.validation.Valid;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月17日 8:52 下午
  * @version V1.0.0
  */
@Controller
@RequestMapping("/admin")
public class TypesController {

    @Autowired
    private TypeService typeService;

    @GetMapping("/types")
    public String types(@PageableDefault(size = 3,sort = {"id"},direction = Sort.Direction.DESC)
                                Pageable pageable, Model model){
         model.addAttribute("page",  typeService.listType(pageable));
        return "admin/types";
    }

    @GetMapping("/types/input")
    public  String input(Model model){
        model.addAttribute("type",new Type());
        return "admin/types-input";
    }

    @GetMapping("/types/{id}/input")
  public String editInput(@PathVariable  Long id, Model model){
        model.addAttribute("type",typeService.getType(id));
        return "admin/types-input";
  }


    @PostMapping("/types")
    public String post(@Valid Type type, BindingResult result, RedirectAttributes attributes){
        //不允许重复校验
     Type type1 =typeService.getTypeByName(type.getName());
      if (type1!=null){
          result.rejectValue("name","nameError","不能添加重复分类");
      }
        if(result.hasErrors()){
         return "admin/types-input";
        }
         Type t = typeService.saveType(type);
        if (t ==null){
           attributes.addFlashAttribute("message","新增失败");
        }else
        {
            attributes.addFlashAttribute("message","新增成功");
        }
        return "redirect:/admin/types";
    }

    @PostMapping("/types/{id}")
    public String editpost(@Valid Type type, BindingResult result,@PathVariable Long id, RedirectAttributes attributes){
        //不允许重复校验
        Type type1 =typeService.getTypeByName(type.getName());
        if (type1!=null){
            result.rejectValue("name","nameError","不能添加重复分类");
        }

        if(result.hasErrors()){
            return "admin/types-input";
        }

        Type t = typeService.updateType(id,type);
        if (t ==null){
            attributes.addFlashAttribute("message","更新失败");
        }else
        {
            attributes.addFlashAttribute("message","更新成功");
        }
        return "redirect:/admin/types";
    }

    @GetMapping("/types/{id}/delete")
    public  String delete(@PathVariable Long id,RedirectAttributes attributes){

        typeService.deleteType(id);
        attributes.addFlashAttribute("message","删除成功");
        return "redirect:/admin/types";
    }

}
