/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.web;

import com.moore.blog.NotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sun.awt.geom.AreaOp;

/**
  * moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年02月27日 1:02 下午
  * @version V1.0.0
  */
@Controller
public class IndexController {

     @GetMapping("/")
    public String index(){
//       String blog =null;
//       if (blog==null){
//           throw  new NotFoundException("博客找不到");
//       }
 //      System.out.println("--------index-------");
      return "index";
    }

    @GetMapping("/blog")
    public String blog(){

         return "blog";
    }

}
