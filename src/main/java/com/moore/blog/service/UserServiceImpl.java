/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.service;

import com.moore.blog.dao.UserRepository;
import com.moore.blog.po.User;
import com.moore.blog.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月07日 12:04 下午
  * @version V1.0.0
  */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private  UserRepository userRepository;

    @Override
    public User checkUser(String username, String password) {

       User user = userRepository.findByUserNameAndPassword(username, MD5Utils.code(password)) ;
       return user;
    }

}
