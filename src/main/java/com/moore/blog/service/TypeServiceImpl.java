/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.service;

import com.moore.blog.NotFoundException;
import com.moore.blog.dao.TypeRepository;
import com.moore.blog.po.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月16日 11:16 下午
  * @version V1.0.0
  */
@Service
public class TypeServiceImpl implements TypeService {
  @Autowired
  private TypeRepository typeRepository;


    @Transactional
    @Override
    public Type saveType(Type type) {
        return typeRepository.save(type);
    }

    @Transactional
    @Override
    public Type getType(Long Id) {
        return typeRepository.findById(Id).get();
    }



    @Transactional
    @Override
    public Page<Type>listType(Pageable pageable){

    return    typeRepository.findAll(pageable);


    }

    @Transactional
    @Override
    public Type updateType(Long id, Type type) {
         Type t = typeRepository.findById(id).get();
         if (t==null){
           throw new NotFoundException("不存在该类型");
         }
      BeanUtils.copyProperties(type,t);

        return typeRepository.save(t) ;
    }

    @Transactional
    @Override
    public void deleteType(Long id) {
      typeRepository.deleteById(id);
    }

  @Transactional
  @Override
  public Type getTypeByName(String name) {
    return typeRepository.findByName(name);
  }
}
