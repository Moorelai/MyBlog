/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.service;

import com.moore.blog.po.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年04月03日 10:14 下午
  * @version V1.0.0
  */

public interface TagService {

    /**
     * 保存
     * @param tag
     * @return
     */
    Tag saveTag(Tag tag);

    /**
     * 查询
     * @param Id
     * @return
     */
    Tag getTag(Long Id);


    Tag getTagByName(String name);

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Tag> listTag(Pageable pageable);


    /**
     * 更新
     * @param id
     * @param tag
     * @return
     */
    Tag updateTag(Long id,Tag tag);

    /**
     * 删除
     * @param id
     */
    void deleteTag(Long id);


}


   