/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.service;

import com.moore.blog.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月16日 11:10 下午
  * @version V1.0.0
  */
public interface TypeService {
    /**
     * 保存
     * @param type
     * @return
     */
    Type saveType(Type type);

    /**
     * 查询
      * @param Id
     * @return
     */
    Type getType(Long Id);

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Type> listType(Pageable pageable);

    /**
     * 更新
     * @param id
     * @param type
     * @return
     */
    Type updateType(Long id,Type type);

    /**
     * 删除
     * @param id
     */
    void deleteType(Long id);

    Type getTypeByName(String name);
}

   