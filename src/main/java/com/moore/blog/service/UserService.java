/**
 *    
 *  * Copyright © 2014 - 2019 Meidd. All Rights Reserved. 美得得科技（深圳）有限公司 版权所有
 *  
 */
package com.moore.blog.service;

import com.moore.blog.po.User;

/**
  * Moore
  * 
  * @author 作者、输入自己的英文名
  * @date 2021年03月07日 12:03 下午
  * @version V1.0.0
  */
public interface UserService {

    User checkUser(String username, String password);

}

   